#data/helpers
The helpers folder will contain helper functions and interfaces, which can be
used anywhere in the application. These can be functions to simplify working with the DOM,
third party APIs, etc. One of the helpers will be _scheduleHelper.js_, which is
an interface for creating and managing a schedule.