# middlewares 
In the middlewares folder, intermediate functions will be implemented that will perform the necessary actions with the 
request before / after processing by the route.

In this project, middlewares will be implemented to validate authorization / registration data, as well as to add 
a user role to the request. Additional validations are possible.