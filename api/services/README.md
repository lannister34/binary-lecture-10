#services
The services folder will contain all the services in which all the business logic of the application will be 
implemented.

The system has 11 services:
* Bar Service. Implements the logic of the bar. Responsible for CRUD of drinks / dishes, menu formation, order creation.
* Achievement Service. Implements the achievement system. Responsible for CRUD achievements and tracking their progress 
by users.
* Blog Service. Implements the logic of the blog (news feed). Responsible for CRUD articles.
* Equipment Service. Implements the logic for accounting for equipment. Responsible for storing information about 
the availability of equipment and its condition, rent/sale of equipment.
* Mail Service. Responsible for sending email and alerts. Uses an external mail service.
* Authorization Service. Responsible for registration and authorization in the system.
* User Service. Responsible for CRUD users, saving information about them and their actions within the wake park, 
distribution of roles in the system.
* Payment Service. Implements an internal payment system. Responsible for replenishing the account and spending funds. 
Uses external API.
* Massage Service. Responsible for managing the schedule of massage sessions, booking sessions. Implements the Schedule 
class (_/helpers/scheduleHelper.js_).
* Schedule Service. Responsible for managing the work schedule of employees. Implements the Schedule class (_/helpers/scheduleHelper.js_).
* Training Service. Responsible for managing the training schedule, booking training. Implements the Schedule class (_/helpers/scheduleHelper.js_).
