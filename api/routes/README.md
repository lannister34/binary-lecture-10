# routes 
If the Single Page Application is implemented, routes directory is not needed in this application, 
because all requests to the server will be handled by Socket.

Otherwise, modules for processing incoming requests and forming a response to them will be implemented here 
(the requested html page will be returned, the rest of the route logic will still be implemented in the Socket).

The entry point will be in _/routes/index.js_, where the required route module will be called.