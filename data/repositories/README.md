#data/repositories
The repositories folder will provide interfaces for direct interaction with
a database: search, record, update, delete data. All repositories will be
inherited from the _base.repository.js_ repository, which implements the base methods
access to the database.