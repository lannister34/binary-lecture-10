# Backend structure #

For this application i chose _service-oriented architecture_. In this case, it has some advantages:

* this architecture allows developing and implementation modules in parallel, which will speed up the development
and allow to use the application immediately, as soon as at least one module is ready

* the service structure allows you to expand the application and add new functionality without significant changes 
to the already implemented functionality

* the independence of the modules allows more flexible selection of the necessary technologies for a specific service

The application will be implemented using the WebSocket protocol. This is due to the parallel use of the application by 
several users at once and the need to receive and update information in real time. 

**Application architecture:**

_https://i.ibb.co/FgPrYzL/Architecture.png_

![/Architecture.png](https://i.ibb.co/FgPrYzL/Architecture.png)

The system is divided into 11 services depending on the area of application. _Подробнее в /services/README.md._


**Classification of system users by roles:** 

* Administrator

* Coach

* Barman

* Client 

**Class diagram:** 

_https://i.ibb.co/ZNT82MF/Class-Diagram.png_

![/Class-Diagram.png](https://i.ibb.co/ZNT82MF/Class-Diagram.png)